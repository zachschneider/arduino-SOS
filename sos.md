```python
int ledPin = 13;
int myArray[]={300,300,300,600,600,600,300,300,300};

void setup(){
  pinMode(ledPin,OUTPUT);
}

void loop(){
  for (int i=0; i<9; i++) {
    digitalWrite(ledPin,HIGH);
    delay(myArray[i]);
    digitalWrite(ledPin,LOW);
    delay(300);
  }
  delay(1000);
}
```